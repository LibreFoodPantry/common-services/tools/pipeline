# LFP DevOps Pipeline

Provides a GitLab CI pipeline for building and releasing a project,
including a Docker image. You can use this pipeline even if your project
doesn't produce a Docker image.

Version numbers follow SemVer and are automatically generated based on
commit messages that follow Conventional Commit. Because it is important
to use Conventional Commit messages for this pipeline to work properly,
it also checks that Conventional Commits is being followed and prevents
mergers if they are not.

When a release is created, if a Docker image exists for the default branch,
it will tag it with teh following tags:

* `latest`
* `MAJOR`
* `MAJOR.MINOR`
* `MAJOR.MINOR.PATCH`

This allows clients to pin to a version number but allow for updates based
on that project's comfort level with adopting new releases. For example,
by pinning to 3.2, the client would receive all new releases that start
with 3.2 (e.g., 3.2.4, 3.2.5, etc.).

## Usage

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
