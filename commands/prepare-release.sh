#!/usr/bin/env bash

set -e

mkdir -p ./artifacts/release
echo "# Pipeline ${1}" >> ./artifacts/release/pipeline.yml
cat ./source/gitlab-ci/pipeline.yml >> ./artifacts/release/pipeline.yml
